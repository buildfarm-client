exim-build-farm-client
======================

Client to build Exim and report results to a central server for developers to look for problems or regressions.

Refer to the [Installation](./wiki/Installation.md) wiki page for detailed instructions to get an [Exim Build Farm](https://buildfarm.exim.org/) client up and running.
